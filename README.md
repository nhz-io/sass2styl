<big><h1 align="center">sass2styl</h1></big>

<p align="center">
  <a href="https://npmjs.org/package/sass2styl">
    <img src="https://img.shields.io/npm/v/sass2styl.svg?style=flat-square"
         alt="NPM Version">
  </a>

  <a href="https://coveralls.io/r/nhz-io/sass2styl">
    <img src="https://img.shields.io/coveralls/nhz-io/sass2styl.svg?style=flat-square"
         alt="Coverage Status">
  </a>

  <a href="https://travis-ci.org/nhz-io/sass2styl">
    <img src="https://img.shields.io/travis/nhz-io/sass2styl.svg?style=flat-square"
         alt="Build Status">
  </a>

  <a href="https://npmjs.org/package/sass2styl">
    <img src="http://img.shields.io/npm/dm/sass2styl.svg?style=flat-square"
         alt="Downloads">
  </a>

  <a href="https://david-dm.org/nhz-io/sass2styl.svg">
    <img src="https://david-dm.org/nhz-io/sass2styl.svg?style=flat-square"
         alt="Dependency Status">
  </a>

  <a href="https://github.com/nhz-io/sass2styl/blob/master/LICENSE">
    <img src="https://img.shields.io/npm/l/sass2styl.svg?style=flat-square"
         alt="License">
  </a>
</p>

<p align="center"><big>
sass to stylus conversion
</big></p>


## Install

```sh
npm i -D sass2styl
```

## Usage

```js
import sass2styl from "sass2styl"

sass2styl() // true
```

## License

MIT © [Ishi Ruy](http://github.com/nhz-io)

[npm-url]: https://npmjs.org/package/sass2styl
[npm-image]: https://img.shields.io/npm/v/sass2styl.svg?style=flat-square

[travis-url]: https://travis-ci.org/nhz-io/sass2styl
[travis-image]: https://img.shields.io/travis/nhz-io/sass2styl.svg?style=flat-square

[coveralls-url]: https://coveralls.io/r/nhz-io/sass2styl
[coveralls-image]: https://img.shields.io/coveralls/nhz-io/sass2styl.svg?style=flat-square

[depstat-url]: https://david-dm.org/nhz-io/sass2styl
[depstat-image]: https://david-dm.org/nhz-io/sass2styl.svg?style=flat-square

[download-badge]: http://img.shields.io/npm/dm/sass2styl.svg?style=flat-square
